#-------------------------------------------------
#
# Project created by QtCreator 2015-10-21T11:20:55
#
#-------------------------------------------------

QT       += core gui

TARGET = mynotepad
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
